import numpy as np
import logging, random, tqdm, cv2, pathlib, bbox, math

class KITTIData:
	# Internal representation for KITTI data
	# Scaling of images
	SCALE = 0.25
	# KITTI classes
	CLASSID = {'DontCare' : 0, 'Car' : 1, 'Van': 2, 'Truck' : 3, 'Cyclist' : 4, 'Pedestrian' : 5, 'Tram': 6, 'Person_sitting' : 7, 'Misc' : 8}
	
	def importData(self, images, objects, matricies):
		# Sets data provided externally
		self.matricies = matricies
		self.images = images
		self.objects = objects
		self.details()

	def saveTo(self, path):
		# Saves the internal format to file
		logging.info(f'Saving to directory: {path}/data')
		pathlib.Path(path / 'data').mkdir(parents=True, exist_ok=True)
		np.save(str(path /  'data/kitti-images.npy'), self.images)
		np.save(str(path / 'data/kitti-objects.npy'), self.objects)
		np.save(str(path / 'data/kitti-matricies.npy'), self.matricies)
		logging.info(f'Scaling Images')
		self.images = np.load(str(path / 'data/kitti-images.npy'), mmap_mode='r')
		resizeShape = (self.images.shape[0],) + self.scaleDown(self.images[0]).shape
		normImages = np.lib.format.open_memmap('data/kitti-images-norm.npy', mode='w+', shape=resizeShape)
		for i in tqdm.tqdm(range(normImages.shape[0])):
			normImages[i] = self.scaleDown(self.images[i])
		
		logging.info(f'Normalising Images')
		self.calcImageDistribution(normImages)
		np.save(str(path /  'data/kitti-image-mean.npy'), self.imgMean)
		np.save(str(path /  'data/kitti-image-std.npy'), self.imgStd)
		for i in tqdm.tqdm(range(normImages.shape[0])):
			normImages[i] = (normImages[i] - self.imgMean) / self.imgStd

	def scaleDown(self, image):
		return cv2.resize(image, (0, 0), fx=KITTIData.SCALE, fy=KITTIData.SCALE)

	def loadFrom(self, path):
		# Loads the internal format from file
		logging.info(f'Loading from directory: {path}/data')
		self.images = np.load(str(path / 'data/kitti-images.npy'), mmap_mode='r')
		self.objects = np.load(str(path / 'data/kitti-objects.npy'))
		self.matricies = np.load(str(path / 'data/kitti-matricies.npy'))
		self.normImages = np.load(str(path / 'data/kitti-images-norm.npy'), mmap_mode='r')
		self.details()

	def details(self):
		# Log metadata
		logging.info(f'Dataset Images: {self.images.shape}')
		logging.info(f'Dataset Objects: {self.objects.shape}')
		logging.info(f'Dataset Matricies: {self.matricies.shape}')

	def calcImageDistribution(self, images):
		# Calculates image mean and standard deviation
		self.imgMean = np.mean(images, axis=0)
		self.imgStd = np.zeros_like(self.imgMean)
		for i in range(images.shape[0]):
			self.imgStd += (images[i] - self.imgMean) ** 2

		self.imgStd = np.sqrt(self.imgStd / (images.shape[0] - 1))

	def calcPoints(self, objects):
		# Calculates 3D bounding box points
		boxAddition = np.array([[ 0,  1, -1], 
								[ 0,  1,  1],
								[ 0, -1,  1],
								[ 0, -1, -1],
								[-2,  1, -1], 
								[-2,  1,  1],
								[-2, -1,  1],
								[-2, -1, -1]])
		halfdim = objects[:, 9:12]/2

		points = halfdim[:, np.newaxis] * boxAddition
		points = points[:, :, np.argsort([1, 0, 2])]

		worldPoints = np.zeros_like(points)

		rotation = -objects[:, 15]
		translation = objects[:, 12:15]

		for i in range(points.shape[0]):
			r = rotation[i]
			R = np.array([[np.cos(r), 0, np.sin(r)], [0, 1, 0], [-np.sin(r), 0, np.cos(r)]])
			for j in range(points[i].shape[0]):
				points[i][j] = np.matmul(points[i][j], R)
				worldPoints[i][j] = points[i][j] + translation[i]

		dx1 = points[:, 0, 0]
		dx2 = points[:, 1, 0]
		dz1 = points[:, 0, 2]
		dz2 = points[:, 1, 2]

		x = objects[:, 12]
		y = objects[:, 13]
		z = objects[:, 14]
		h = objects[:, 9]

		reduced = np.array([x, y, z, dx1, dz1, dx2, dz2, h]).T

		return worldPoints, reduced

	def calcPointDistribution(self, points):
		# Calculates 3D mean and standard deviation
		stacked = np.vstack(points)
		pointMean = np.mean(stacked, axis=0)
		pointStd = np.std(stacked, axis=0)
		return pointMean, pointStd

	def calcObjectDistribution(self, objects):
		# Calculates object mean and standard deviation
		objMean = np.mean(objects, axis=0)
		objStd = np.std(objects, axis=0)
		objStd[objStd == 0] = 1

		return objMean, objStd

	def getRandomImageIndex(self):
		# Retrives an image randomly
		return random.randint(0, self.images.shape[0] - 1)

	def getObjects(self, image=None, label=None, occluded=None, truncated=None):
		# Selects objects satisfying parameters
		objects = self.objects
		if image is not None:
			objects = objects[np.where(objects[:, 0] == image)]

		if label is not None:
			objects = objects[np.where(objects[:, 1] == KITTIData.CLASSID[label])]

		if truncated is not None:
			objects = objects[np.where(objects[:, 2] <= truncated)]

		if occluded is not None:
			objects = objects[np.where(objects[:, 3] <= int(occluded))]

		return objects



class KITTIObject:
	# Representation of objects in the KITTI dataset
	def __init__(self, objectrow):
		self.imageID = int(objectrow[0])
		self.classID = int(objectrow[1])
		self.className = list(KITTIData.CLASSID.keys())[self.classID]
		self.truncated = objectrow[2]
		self.occluded = int(objectrow[3])
		self.observedAngle = objectrow[4]
		self.boundingBox = [int(c) for c in list(objectrow[5:9])]
		self.dimensions = list(objectrow[9:12])
		self.location = list(objectrow[12:15])
		self.rotation = objectrow[15]

	def __repr__(self):
		return str(self)

	def __str__(self):
		s  = f'ImageID: {self.imageID} \nClassID: {self.classID} - {self.className}\n'
		s += f'Truncated: {self.truncated} \nOccluded: {self.occluded}\n'
		s += f'Observed Angle: {self.observedAngle}rad \n2DBBox: {self.boundingBox}\n'
		s += f'Dimensions: {self.dimensions} \nLocation: {self.location}\n'
		s += f'YRotation: {self.rotation}rad'
		return s


class KITTIBox:
	# Representation of 3D boxes
	def __init__(self, points, reduced=False, weird=False):
		# If reduced 8D format
		if reduced:
			points = self.expand(points)

		self.points = points

		centre = np.mean(self.points, axis=0)
		self.c = centre

		ldiff = points[0] - points[1]
		self.l = np.linalg.norm(ldiff)

		wdiff = points[1] - points[2]
		if weird:
			wdiff = points[1] - points[3]
		self.w = np.linalg.norm(wdiff)

		self.h = np.max(points[:, 1]) - np.min(points[:, 1])

		self.angle = -np.arctan(ldiff[0] / ldiff[2])
		if weird:
			self.angle += math.pi/2
		self.box = bbox.BBox3D(centre[0], centre[2], centre[1], length=self.l, width=self.w, height=self.h, euler_angles=[0, 0, self.angle])

	def expand(self, points):
		# Converts from 8D to 24D format
		bpred = np.array([[points[3], 0, points[4]], [points[5], 0, points[6]]])
		bpred = np.append(bpred, -bpred, axis=0)
		bpred = np.append(bpred, bpred, axis=0)
		bpred[4:,1] = -points[7]
		bpred += points[0:3]

		return bpred

	def render(self, img, matrix, colour):
		# Draws the box in the image, with provided projection matrix
		img = img.copy()
		points = self.box.p[:, [0, 2, 1]]
		homo = np.concatenate([points, np.ones((points.shape[0],1),dtype=points.dtype)], axis=1)
		proj = np.matmul(homo, matrix.T)
		coords = proj[:, 0:2] / proj[:, 2:3]
		coords = [tuple(c) for c in coords.astype(int)]
		boxEdges = [[0,1], [0,3], [1,2], [2,3], [0,4], [4,5], [1,5], [4,7], [2,6], [6,7], [3,7], [5,6]]
		for edge in boxEdges:
			cv2.line(img, coords[edge[0]], coords[edge[1]], colour, 2)

		for coord in coords:
			cv2.circle(img, coord, 5, colour, -1)

		return img

	def iou(self, b2):
		# Calculates the IOU of 3D boxes
		return bbox.metrics.jaccard_index_3d(self.box, b2.box)
