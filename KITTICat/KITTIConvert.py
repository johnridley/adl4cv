import numpy as np
import logging, cv2, pathlib, tqdm

from .KITTIData import KITTIData

class KITTIConverter:
	# Ingests the provided KITTI dataset for internal use
	def __init__(self, path):
		self.path = pathlib.Path(path)
		self.trainingPath = self.path / 'training'
		self.imageName = 'image_2'
		self.labelName = 'label_2'
		self.calibName = 'calib'
		logging.info(f'Dataset Path: {path}')

	def loadTrain(self):
		# Loads the training set
		sampleCount = len(list((self.trainingPath / self.imageName).glob('*')))
		logging.info(f'Dataset Images: {sampleCount}')

		images = []
		labels = []
		matricies = []

		for identifier in tqdm.tqdm(range(sampleCount)):
			identString = f'{identifier:06}'
			images.append(self.loadImage(self.trainingPath / self.imageName / f'{identString}.png'))
			labels += self.readLabels(self.trainingPath / self.labelName / f'{identString}.txt', identifier)
			matricies.append(self.readCalibration(self.trainingPath / self.calibName / f'{identString}.txt'))

		images = np.stack(images)
		labels = np.stack(labels)
		matricies = np.stack(matricies)

		dataset = KITTIData()
		dataset.importData(images, labels, matricies)

		return dataset

	def loadImage(self, path):
		# Reads an image
		image = cv2.imread(str(path))
		if image is None:
			raise Exception(f'Failed to read image {path}')
		image = cv2.resize(image, (1242, 375)) 	
		return image

	def readLabels(self, path, identifier):
		# Reads provided label information
		with open(path, 'r') as labelFile:
			lines = labelFile.readlines()

		entries = []

		for line in lines:
			line = line.rstrip().split(' ')
			line[0] = KITTIData.CLASSID[line[0]]
			line = [identifier] + [float(val) for val in line]
			entries.append(np.array(line))

		return entries

	def readCalibration(self, path):
		# Reads camera calibration
		with open(path, 'r') as labelFile:
			lines = labelFile.readlines()

		p = lines[2].rstrip().split(' ')[1:]
		p = np.array([float(p) for p in p]).reshape(3, 4)

		return p
