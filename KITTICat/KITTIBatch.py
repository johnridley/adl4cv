import numpy as np
import pathlib, logging, tqdm, cv2

from .KITTIData import KITTIData, KITTIBox

class KITTIBatchGenerator(KITTIData):
	# Generates batches from the KITTI dataset

	# Types of attributes to include, and types of batches
	AVAILIABLE = ['images', 'full-images', 'matricies', 'labels', '2D', '3D', 'heatmap', 'points', 'reduced-points']
	ROLE = ['train', 'validate', 'test']
	def __init__(self, objects, data, trainsplit, valsplit, selector=None):
		self.objects = objects
		# Internal size, only used for preprocessing
		self.size = 500
		self.images = data.images
		self.matricies = data.matricies
		self.normImages = data.normImages

		# Calculate split indicies
		self.trainEnd = int(self.objects.shape[0] * trainsplit)
		self.valEnd = int(((self.objects.shape[0] - self.trainEnd) / 2) + self.trainEnd)

		# Preprocessing
		trainObjects = self.objects[:self.trainEnd]
		self.objMean, self.objStd = self.calcObjectDistribution(trainObjects)
		self.normObjects = (self.objects - self.objMean) / self.objStd

		self.points, self.reducedPoints = self.calcPoints(self.objects)
		self.pointMean, self.pointStd = self.calcPointDistribution(self.points[:self.trainEnd])

		self.reducedPointMean, self.reducedPointStd = self.calcPointDistribution(self.reducedPoints[:self.trainEnd])

		stacked = np.vstack(self.points)
		stacked = (stacked - self.pointMean) / self.pointStd
		self.normPoints = stacked.reshape(self.points.shape)

		self.normReducedPoints = (self.reducedPoints - self.reducedPointMean) / self.reducedPointStd

		# Check requested attributes
		if selector is None:
			self.selected = KITTIBatchGenerator.AVAILIABLE
		else:
			for item in selector:
				if item not in KITTIBatchGenerator.AVAILIABLE:
					raise ValueError(f'Invalid batch type option: {item}')

			self.selected = selector

		self.asRole(KITTIBatchGenerator.ROLE[0])


	def asRole(self, role):
		# Select the appropriate indicies for the specified role
		if role in KITTIBatchGenerator.ROLE:
			self.role = role
		else:
			raise ValueError(f'Invalid role option: {item}')

		if role == 'train':
			self.minIndex = 0
			self.maxIndex = self.trainEnd

		elif role == 'validate':
			self.minIndex = self.trainEnd
			self.maxIndex = self.valEnd

		else:
			self.minIndex = self.valEnd
			self.maxIndex = self.objects.shape[0]


	def numObjects(self):
		return self.maxIndex - self.minIndex

	def __iter__(self):
		self.index = self.minIndex
		self.batchIndex = 0
		return self

	def __next__(self):
		# Iterate through samples
		if self.index >= self.maxIndex - 1:
			raise StopIteration

		targetIndex = self.index + self.size
		if targetIndex >= self.maxIndex:
			targetIndex = self.maxIndex - 1

		batch = self.buildBatch(self.index, targetIndex)
		self.index = targetIndex

		startBatchIndex = self.batchIndex
		targetBatchIndex = self.batchIndex + self.size
		if targetBatchIndex >= self.numObjects():
			targetBatchIndex = self.numObjects() - 1
		self.batchIndex = targetBatchIndex

		return batch, startBatchIndex, targetBatchIndex

	def __len__(self):
		return int(self.numObjects() / self.size)

	def buildBatch(self, startindex, endindex):
		# Create a batch within the index bounds
		# Append the suitable attributes
		batch = {}
		objectSubset = self.objects[startindex:endindex]
		imageIDs = objectSubset[:, 0].astype(int)
		batch['imageids'] = imageIDs

		if 'full-images' in self.selected:
			batch['images'] = self.images[imageIDs]

		if 'matricies' in self.selected:
			batch['matricies'] = self.matricies[imageIDs]

		if 'labels' in self.selected:
			batch['labels'] = objectSubset[:, 1]

		if '2D' in self.selected:
			batch['2DBoxes'] = objectSubset[:, 5:9]

		if '3D' in self.selected:
			batch['observedAngle'] = objectSubset[:, 4]
			batch['dimensions'] = objectSubset[:, 9:12]
			batch['location'] = objectSubset[:, 12:15]
			batch['rotation'] = objectSubset[:, 15]

		if 'points' in self.selected:
			batch['points'] = self.points[startindex:endindex]
			batch['norm-points'] = self.normPoints[startindex:endindex]

		if 'reduced-points' in self.selected:
			batch['reduced-points'] = self.reducedPoints[startindex:endindex]
			batch['norm-reduced-points'] = self.normReducedPoints[startindex:endindex]

		normObjectSubset = self.normObjects[startindex:endindex]
		if 'images' in self.selected:
			batch['norm-images'] = self.normImages[imageIDs]

		if '2D' in self.selected:
			batch['norm-2DBoxes'] = normObjectSubset[:, 5:9]

		if '3D' in self.selected:
			batch['norm-observedAngle'] = normObjectSubset[:, 4]
			batch['norm-dimensions'] = normObjectSubset[:, 9:12]
			batch['norm-location'] = normObjectSubset[:, 12:15]
			batch['norm-rotation'] = normObjectSubset[:, 15]

		if 'heatmap' in self.selected:
			heatmap = np.zeros(batch['norm-images'].shape[0:3])
			for i in range(heatmap.shape[0]):
				canvas = np.zeros(self.images[0].shape[0:2])
				bbox = [int(c) for c in batch['2DBoxes'][i]]
				canvas[bbox[1]:bbox[3], bbox[0]:bbox[2]] = 1
				heatmap[i] = self.scaleDown(canvas)

			batch['heatmap'] = heatmap

		return batch


class KITTIBatchWriter():
	# Writes KITTI batches for later use
	def __init__(self, generator, name):
		pathlib.Path('batches').mkdir(parents=True, exist_ok=True)
		pathlib.Path(f'batches/{name}').mkdir(parents=True, exist_ok=True)
		self.generator = generator
		self.rows = generator.numObjects()
		self.batchName = name

	def write(self):
		init = False
		np.save('batches/batch-mean.npy', self.generator.objMean)
		np.save('batches/batch-std.npy', self.generator.objStd)
		np.save('batches/point-mean.npy', self.generator.pointMean)
		np.save('batches/point-std.npy', self.generator.pointStd)
		np.save('batches/reduced-point-mean.npy', self.generator.reducedPointMean)
		np.save('batches/reduced-point-std.npy', self.generator.reducedPointStd)
		for batch, startIndex, targetIndex in tqdm.tqdm(self.generator):
			if not init:
				self.initialise(batch)
				init = True

			for name, val in self.files.items():
				self.files[name][startIndex:targetIndex] = batch[name] 

	def initialise(self, batch):
		self.files = {}
		for name, val in batch.items():
			shape = (self.rows,) + val.shape[1:]
			self.files[name] = np.lib.format.open_memmap(f'batches/{self.batchName}/{name}.npy', mode='w+', shape=shape)

class KITTIBatchReader():
	# Reads saved KITTI batches
	def __init__(self, size, name):
		p = pathlib.Path(f'batches/{name}').glob('*')
		files = {x.stem:x for x in p if x.is_file()}

		self.batches = {key: np.load(str(path), mmap_mode='r') for key, path in files.items()}
		self.objMean = np.load('batches/batch-mean.npy')
		self.objStd = np.load('batches/batch-std.npy')
		self.pointMean = np.load('batches/point-mean.npy')
		self.pointStd = np.load('batches/point-std.npy')
		self.reducedPointMean = np.load('batches/reduced-point-mean.npy')
		self.reducedPointStd = np.load('batches/reduced-point-std.npy')

		self.size = size
		self.items = self.batches[list(self.batches.keys())[0]].shape[0]

	def __iter__(self):
		self.index = 0
		self.batchNum = 0
		return self

	def __next__(self):
		if self.index >= self.items - 1:
			raise StopIteration

		startIndex = self.index
		targetIndex = self.index + self.size
		if targetIndex >= self.items:
			targetIndex = self.items - 1

		batch = self.buildBatch(self.index, targetIndex)
		self.index = targetIndex

		self.batchNum += 1

		return batch

	def __len__(self):
		return int(self.items / self.size) 

	def buildBatch(self, startindex, endindex):
		batch = {key: item[startindex:endindex] for key, item in self.batches.items()}
		return batch


class KITTIValidator():
	# Tools used to validate samples
	def validate(self, inputs, valid, batchgen, reduced=False, weird=False):
		# Calculates KITTI scores, IOU and boxes
		generatedBoxes = []
		gtBoxes = []
		# If reduced 8D formulation is used
		if reduced:
			samples = inputs.shape[0]
			scores = np.zeros((samples,))
			ious = np.zeros_like(scores)
			for i in range(samples):
				corrected = (inputs[i] * batchgen.reducedPointStd) + batchgen.reducedPointMean
				generated = KITTIBox(corrected, reduced=True)
				generatedBoxes.append(generated)
				gt = KITTIBox(valid[i])
				gtBoxes.append(gt)
				ious[i] = gt.iou(generated)
				scores[i] = 1 if ious[i] > 0.7 else 0
		# Else 24D formulation is used
		else:
			samples = inputs.shape[0]
			scores = np.zeros((samples,))
			ious = np.zeros_like(scores)
			for i in range(samples):
				corrected = (inputs[i].reshape(8, 3) * batchgen.pointStd) + batchgen.pointMean
				generated = KITTIBox(corrected, weird=weird)
				generatedBoxes.append(generated)
				gt = KITTIBox(valid[i])
				gtBoxes.append(gt)
				ious[i] = gt.iou(generated)
				scores[i] = 1 if ious[i] > 0.7 else 0

		return scores, ious, generatedBoxes, gtBoxes

