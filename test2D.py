import numpy as np
import torch, sys, pathlib, tqdm

from KITTICat.KITTIBatch import KITTIBatchReader, KITTIValidator, KITTIData

# Load the model specified by the command line argument
model = torch.load(sys.argv[1], map_location='cpu').to('cpu')

# Converts 2D bounding boxes to heatmaps
def boxToHeatmap(boxes, shape):
	heatmaps = np.zeros(shape)
	scaled = (boxes * 0.25).astype(int)
	heatmaps[scaled[1]:scaled[3], scaled[0]:scaled[2]] = 1
	    
	return heatmaps

# Load KITTI dataset
data = KITTIData()
data.loadFrom(pathlib.Path('.'))

# Load batch normalisation
batchMean = np.load('batches/batch-mean.npy')[5:9]
batchStd = np.load('batches/batch-std.npy')[5:9]

# Go through image samples detected
for identifier in tqdm.tqdm(range(6001, 7481)):
	detections = np.load(f'2doutput/{identifier:06}.pkl')
	results = np.zeros((len(detections), 24))
	for num, detection in enumerate(detections):
		box = np.array(detection)[1:]
		heatmap = boxToHeatmap(box, data.normImages[0].shape[0:2])
		heatmap = torch.from_numpy(np.expand_dims(np.expand_dims(heatmap, axis=3), axis=0)).permute(0, 3, 1, 2).float()
		with torch.no_grad():
			image = torch.from_numpy(data.normImages[identifier:identifier+1]).permute(0, 3, 1, 2).float()
			outputs = model(image, heatmap)
			results[num, :] = outputs.cpu().numpy()

	# Output the 3D lifted descriptor
	np.save(f'2dlifted/{identifier:06}.npy', results)




