import pathlib, torch, sys

from KITTICat.KITTIBatch import KITTIBatchReader, KITTIValidator, KITTIData
from LiftVision.viewer import LiftVision
from LiftVision.observer import LiftDriver

from Models.Prebuilt import *

# Load the model specified in the command line argument
model = torch.load(sys.argv[1], map_location='cpu').to('cpu')

# Load the dataset
data = KITTIData()
data.loadFrom(pathlib.Path('.'))

# Load test batches
batch = KITTIBatchReader(2542, 'test')

# Setup loading model
driver = LiftDriver(model, data, batch, KITTIValidator())

# Run the visualiser
app = LiftVision(driver)
app.run()