import torch
import torch.nn as nn
import torch.nn.functional as f

import torchvision.models as models 

# Preexisting architectures, modified for lifting

class Resnext(nn.Module):
	def __init__(self):
		super(Resnext, self).__init__()

		self.resnet = models.resnext50_32x4d(pretrained=False)
		self.resnet.conv1 = nn.Conv2d(in_channels=4, out_channels=64, kernel_size=7, stride=2, padding=3, bias=False)
		self.resnet.fc = Identity()

		self.flatshape = 2048
		self.fc = nn.Sequential(
				nn.Linear(self.flatshape, 256),
				nn.ReLU(),
				nn.Linear(256, 128),
				nn.ReLU(),
				nn.Linear(128, 24)
			)

	def forward(self, img, heatmap):
		x = torch.cat((img, heatmap), dim=1)

		x = self.resnet(x)
		x = self.fc(x)

		return x


class Identity(nn.Module):
	def __init__(self):
		super(Identity, self).__init__()
		
	def forward(self, x):
		return x

class PrebuiltRed(nn.Module):
	def __init__(self):
		super(PrebuiltRed, self).__init__()

		self.resnet = models.resnet34(pretrained=False)
		self.resnet.conv1 = nn.Conv2d(in_channels=4, out_channels=64, kernel_size=3, stride=2, bias=False)
		self.resnet.fc = Identity()

		self.flatshape = 512
		self.fc1 = nn.Linear(self.flatshape, 256)
		self.fc2 = nn.Linear(256, 128)
		self.fc3 = nn.Linear(128, 8)


	def forward(self, img, heatmap):
		x = torch.cat((img, heatmap), dim=1)

		x = self.resnet(x)
		x = f.relu(self.fc1(x))
		x = f.relu(self.fc2(x))
		x = self.fc3(x)

		return x


class Prebuilt(nn.Module):
	def __init__(self):
		super(Prebuilt, self).__init__()

		self.resnet = models.resnet34(pretrained=False)
		self.resnet.conv1 = nn.Conv2d(in_channels=4, out_channels=64, kernel_size=3, stride=2, bias=False)
		self.resnet.fc = Identity()

		self.flatshape = 512
		self.fc1 = nn.Linear(self.flatshape, 256)
		self.fc2 = nn.Linear(256, 128)
		self.fc3 = nn.Linear(128, 24)


	def forward(self, img, heatmap):
		x = torch.cat((img, heatmap), dim=1)

		x = self.resnet(x)
		x = f.relu(self.fc1(x))
		x = f.relu(self.fc2(x))
		x = self.fc3(x)

		return x
