import torch
import torch.nn as nn
import torch.nn.functional as f

class Generator(nn.Module):
	def __init__(self):
		super(Generator, self).__init__()

		self.conv1 = nn.Conv2d(in_channels=4, out_channels=4, kernel_size=7)

		self.res1 = ResBlock(4, 3)
		self.conv2 = nn.Conv2d(in_channels=4, out_channels=8, kernel_size=3)
		self.res2 = ResBlock(8, 3)
		self.conv3 = nn.Conv2d(in_channels=8, out_channels=8, kernel_size=3)
		self.res3 = ResBlock(8, 3)
		self.conv4 = nn.Conv2d(in_channels=8, out_channels=16, kernel_size=3)

		self.flatshape = 3808
		self.fc1 = nn.Linear(self.flatshape, 1024)
		self.fc1_bn = nn.BatchNorm1d(1024)
		self.fc2 = nn.Linear(1024, 256)
		self.fc2_bn = nn.BatchNorm1d(256)
		self.fc3 = nn.Linear(256, 24)
		# self.fc3_bn = nn.BatchNorm1d(256)
		# self.fc4 = nn.Linear(256, 7)



	def forward(self, img, heatmap):
		x = torch.cat((img, heatmap), dim=1)
		x = f.leaky_relu(self.conv1(x), negative_slope=0.2)
		x = self.res1(x)
		x = f.avg_pool2d(x, kernel_size=3, stride=2)
		x = f.leaky_relu(self.conv2(x), negative_slope=0.2)
		x = self.res2(x)
		x = f.avg_pool2d(x, kernel_size=3, stride=2)
		x = f.leaky_relu(self.conv3(x), negative_slope=0.2)
		x = self.res3(x)
		x = f.leaky_relu(self.conv4(x), negative_slope=0.2)

		x = f.avg_pool2d(x, kernel_size=3, stride=2)

		# print(x.shape)
		x = x.view(-1, self.flatshape)
		# print(x.shape)

		x = f.leaky_relu(self.fc1_bn(self.fc1(x)), negative_slope=0.2)
		x = f.leaky_relu(self.fc2_bn(self.fc2(x)), negative_slope=0.2)
		x = self.fc3(x)

		return x


class Discriminator(nn.Module):
	def __init__(self):
		super(Discriminator, self).__init__()

		self.conv1 = nn.Conv2d(in_channels=4, out_channels=4, kernel_size=7)

		self.res1 = ResBlock(4, 3)
		self.conv2 = nn.Conv2d(in_channels=4, out_channels=8, kernel_size=3)
		self.res2 = ResBlock(8, 3)
		self.conv3 = nn.Conv2d(in_channels=8, out_channels=8, kernel_size=3)
		self.res3 = ResBlock(8, 3)
		self.conv4 = nn.Conv2d(in_channels=8, out_channels=16, kernel_size=3)

		self.flatshape = 3808
		self.fc1 = nn.Linear(self.flatshape, 1024)
		self.fc1_bn = nn.BatchNorm1d(1024)
		self.fc2 = nn.Linear(1024, 256)
		
		self.fc2_bn = nn.BatchNorm1d(256)
		self.fc3 = nn.Linear(256, 24)
		self.fc3_bn = nn.BatchNorm1d(48)
		self.fc4 = nn.Linear(48, 1)



	def forward(self, img, heatmap, boxes):
		x = torch.cat((img, heatmap), dim=1)
		x = f.leaky_relu(self.conv1(x), negative_slope=0.2)
		x = self.res1(x)
		x = f.avg_pool2d(x, kernel_size=3, stride=2)
		x = f.leaky_relu(self.conv2(x), negative_slope=0.2)
		x = self.res2(x)
		x = f.avg_pool2d(x, kernel_size=3, stride=2)
		x = f.leaky_relu(self.conv3(x), negative_slope=0.2)
		x = self.res3(x)
		x = f.leaky_relu(self.conv4(x), negative_slope=0.2)

		x = f.avg_pool2d(x, kernel_size=3, stride=2)

		# print(x.shape)
		x = x.view(-1, self.flatshape)
		
		# print(x.shape)

		x = f.leaky_relu(self.fc1_bn(self.fc1(x)), negative_slope=0.2)
		x = self.fc2(x)



		x = f.leaky_relu(self.fc2_bn(x), negative_slope=0.2)

		x = self.fc3_bn(torch.cat((self.fc3(x), boxes), dim=1))

		x = torch.sigmoid(self.fc4(x))

		return x

class ResBlock(nn.Module):
	def __init__(self, channels, kernel):
		super(ResBlock, self).__init__()
		padding = int((kernel-1)/2)
		self.conv1 = nn.Conv2d(channels, channels, kernel, padding=padding)
		self.conv1_bn = nn.BatchNorm2d(channels)
		self.conv2 = nn.Conv2d(channels, channels, kernel, padding=padding)
		self.conv2_bn = nn.BatchNorm2d(channels)

	def forward(self, x):
		residual = x

		x = f.leaky_relu(self.conv1_bn(self.conv1(x)), negative_slope=0.2)
		x = self.conv2_bn(self.conv2(x))

		x += residual

		return x
