# nested inception small
import torch.nn as nn
import torch

class desc(nn.Module):
    
    def __init__(self):
        super(desc, self).__init__()
        self.input_channels = 4
        self.output_channels_ConvInitial = 16
        self.output_channels_Inception = 32
        
        # 612 X 180 X 2
        self.conv1 = ConvInitial(self.input_channels,self.output_channels_ConvInitial,1,1,0) # 612 X 180 X 8
        self.conv2 = ConvInitial(self.input_channels,self.output_channels_ConvInitial,3,2,1) # 306 X 90 X 8
        self.conv3 = ConvInitial(self.input_channels,self.output_channels_ConvInitial,5,3,2) # 204 X 60 X 8
        self.conv4 = ConvInitial(self.input_channels,self.output_channels_ConvInitial,7,4,3) # 153 X 45 X 8
        
        self.inception = Inception(self.output_channels_ConvInitial,self.output_channels_Inception) # p X q X (16*4)
        
        self.convA = ConvInitial(self.output_channels_Inception*4,self.output_channels_Inception*4,5,3,2) # 204 X 60 X 64
        self.convB = ConvInitial(self.output_channels_Inception*4,self.output_channels_Inception*4,3,2,1) # 153 X 45 X 64
        
        self.convC = nn.Sequential(
                                    ConvInitial(self.output_channels_Inception*8,self.output_channels_Inception*4,5,3,2),
                                    ConvInitial(self.output_channels_Inception*4,self.output_channels_Inception*4,3,2,1),
                                    ConvInitial(self.output_channels_Inception*4,self.output_channels_Inception*4,3,2,1)
                                    ) # 17 X 5 X 64
        self.convD = nn.Sequential(
                                    ConvInitial(self.output_channels_Inception*8,self.output_channels_Inception*4,3,2,1),
                                    ConvInitial(self.output_channels_Inception*4,self.output_channels_Inception*4,3,2,1),
                                    ConvInitial(self.output_channels_Inception*4,self.output_channels_Inception*4,3,1,1)
                                    ) # 17 X 5 X 64
        
         # 17 X 5 X (128*2)
        self.fc = nn.Sequential(
                                nn.Linear(18816,1024,bias=True),
                                nn.Dropout(),
                                nn.Linear(1024,256,bias=True),
                                nn.Dropout(),
                                nn.Linear(256,24)
                                )   
    def forward(self, img, heatmap):
        img = torch.cat((img, heatmap), dim=1)
        c1 = self.conv1(img); c2 = self.conv2(img); c3 = self.conv3(img); c4 = self.conv4(img)
        c1 = self.inception(c1); c2 = self.inception(c2); c3 = self.inception(c3); c4 = self.inception(c4)
        c1 = self.convA(c1); c2 = self.convB(c2)
        a = [c1, c3]; a = torch.cat(a, 1); b = [c2, c4]; b = torch.cat(b, 1)
        c = self.convC(a); d = self.convD(b)
        e = [c.view(c.shape[0], -1), d.view(d.shape[0], -1)]
        e = torch.cat(e, 1)
        # print(e.shape)
        d_img = self.fc(e)
        return d_img
    @property
    def is_cuda(self):
        return next(self.parameters()).is_cuda

class ConvInitial(nn.Module):
    def __init__(self, ip_channel, op_channel, kernel, stride, padding):
        super(ConvInitial, self).__init__()
        self.conv_initial = nn.Sequential(
                                        nn.Conv2d(ip_channel,op_channel,kernel,stride=stride,padding=padding,bias=False),
                                        nn.BatchNorm2d(op_channel),
                                        nn.ReLU(inplace=True)
                                        )
    def forward(self, x):
        x = self.conv_initial(x)
        return x

class Inception(nn.Module):
    def __init__(self, in_channel, out_channel):
        super(Inception, self).__init__()
        self.branch1 = nn.Sequential(
                                    ConvInitial(in_channel,out_channel,1,stride=1,padding=0),
                                    ConvInitial(out_channel,out_channel,3,stride=1,padding=1)
                                    )
        self.branch2 = nn.Sequential(
                                    ConvInitial(in_channel,out_channel,3,stride=1,padding=1),
                                    ConvInitial(out_channel,out_channel,3,stride=1,padding=1)
                                    )
        self.branch3 = nn.Sequential(
                                    ConvInitial(in_channel,out_channel,3,stride=1,padding=1),
                                    ConvInitial(out_channel,out_channel,3,stride=1,padding=1)
                                    )
        self.branch4 = nn.Sequential(
                                    ConvInitial(in_channel,out_channel,5,stride=1,padding=2),
                                    ConvInitial(out_channel,out_channel,3,stride=1,padding=1)
                                    )
    def forward(self, x):
        b1 = self.branch1(x); b2 = self.branch2(x); b3 = self.branch3(x); b4 = self.branch4(x)
        outputs = [b1, b2, b3, b4]
        return torch.cat(outputs, 1)
