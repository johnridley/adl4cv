import torch
import torch.nn as nn
import torch.nn.functional as f

# Simple custom architecture for 2D lifting

class Naive(nn.Module):
	def __init__(self):
		super(Naive, self).__init__()

		self.conv1 = nn.Conv2d(in_channels=4, out_channels=16, kernel_size=5)

		self.res1 = ResBlock(16, 3)
		self.res12 = ResBlock(16, 3)
		self.res13 = ResBlock(16, 3)
		self.res14 = ResBlock(16, 3)
		self.res15 = ResBlock(16, 3)
		self.res16 = ResBlock(16, 3)

		self.conv2 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3)
		self.conv2_bn = nn.BatchNorm2d(32)

		self.res2 = ResBlock(32, 3)
		self.res22 = ResBlock(32, 3)
		self.res23 = ResBlock(32, 3)
		self.res24 = ResBlock(32, 3)
		self.res25 = ResBlock(32, 3)
		self.res26 = ResBlock(32, 3)

		self.conv3 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3)
		self.conv3_bn = nn.BatchNorm2d(64)

		self.res3 = ResBlock(64, 3)
		self.res32 = ResBlock(64, 3)
		self.res33 = ResBlock(64, 3)
		self.res34 = ResBlock(64, 3)
		self.res35 = ResBlock(64, 3)
		self.res36 = ResBlock(64, 3)

		self.conv4 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3)
		self.conv4_bn = nn.BatchNorm2d(128)

		self.res4 = ResBlock(128, 3)
		self.res42 = ResBlock(128, 3)
		self.res43 = ResBlock(128, 3)
		self.res44 = ResBlock(128, 3)
		self.res45 = ResBlock(128, 3)
		self.res46 = ResBlock(128, 3)

		self.conv5 = nn.Conv2d(in_channels=128, out_channels=256, kernel_size=3)
		self.conv5_bn = nn.BatchNorm2d(256)

		self.flatshape = 1792 #13056 
		self.fc1 = nn.Linear(self.flatshape, 1024)
		self.fc1_bn = nn.BatchNorm1d(1024)
		self.fc2 = nn.Linear(1024, 512)
		self.fc2_bn = nn.BatchNorm1d(512)
		self.fc3 = nn.Linear(512, 256)
		self.fc3_bn = nn.BatchNorm1d(256)
		self.fc4 = nn.Linear(256, 24)


	def forward(self, img, heatmap):
		x = torch.cat((img, heatmap), dim=1)
		# print(x.shape)
		x = f.relu(f.max_pool2d(self.conv1(x), 2))

		x = f.relu(self.res1(x))
		x = f.relu(self.res12(x))
		x = f.relu(self.res13(x))
		x = f.relu(self.res14(x))
		x = f.relu(self.res15(x))
		x = f.relu(self.res16(x))

		x = f.relu(f.max_pool2d(self.conv2_bn(self.conv2(x)), 2))

		x = f.relu(self.res2(x))
		x = f.relu(self.res22(x))
		x = f.relu(self.res23(x))
		x = f.relu(self.res24(x))
		x = f.relu(self.res25(x))
		x = f.relu(self.res26(x))

		x = f.relu(f.max_pool2d(self.conv3_bn(self.conv3(x)), 2))

		x = f.relu(self.res3(x))
		x = f.relu(self.res32(x))
		x = f.relu(self.res33(x))
		x = f.relu(self.res34(x))
		x = f.relu(self.res35(x))
		x = f.relu(self.res36(x))

		x = f.relu(f.max_pool2d(self.conv4_bn(self.conv4(x)), 2))

		x = f.relu(self.res4(x))
		x = f.relu(self.res42(x))
		x = f.relu(self.res43(x))
		x = f.relu(self.res44(x))
		x = f.relu(self.res45(x))
		x = f.relu(self.res46(x))

		x = f.relu(f.max_pool2d(self.conv5_bn(self.conv5(x)), 2))
		
		# print(x.shape)
		x = x.view(-1, self.flatshape)
		# print(x.shape)
		x = f.relu(self.fc1_bn(self.fc1(x)))
		x = f.relu(self.fc2_bn(self.fc2(x)))
		# x = self.fc2(x)
		x = f.relu(self.fc3_bn(self.fc3(x)))
		x = self.fc4(x)

		return x


class ResBlock(nn.Module):
	def __init__(self, channels, kernel):
		super(ResBlock, self).__init__()
		padding = int((kernel-1)/2)
		self.conv1 = nn.Conv2d(channels, channels, kernel, padding=padding)
		self.conv1_bn = nn.BatchNorm2d(channels)
		self.conv2 = nn.Conv2d(channels, channels, kernel, padding=padding)
		self.conv2_bn = nn.BatchNorm2d(channels)

	def forward(self, x):
		residual = x

		x = f.relu(self.conv1_bn(self.conv1(x)))
		x = self.conv2_bn(self.conv2(x))

		x += residual

		return x



class Naive1(nn.Module):
	def __init__(self):
		super(Naive1, self).__init__()

		self.conv1 = nn.Conv2d(in_channels=4, out_channels=8, kernel_size=5)
		self.conv2 = nn.Conv2d(in_channels=8, out_channels=16, kernel_size=5)
		self.conv2_bn = nn.BatchNorm2d(16)
		self.conv3 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3)
		self.conv3_bn = nn.BatchNorm2d(32)

		self.flatshape = 10368
		self.fc1 = nn.Linear(self.flatshape, 1024)
		self.fc1_bn = nn.BatchNorm1d(1024)
		self.fc2 = nn.Linear(1024, 24)
		# self.fc2_bn = nn.BatchNorm1d(1024)
		# self.fc3 = nn.Linear(1024, 256)
		# self.fc3_bn = nn.BatchNorm1d(256)
		# self.fc4 = nn.Linear(256, 7)


	def forward(self, img, heatmap):
		x = torch.cat((img, heatmap), dim=1)
		# print(x.shape)
		x = f.relu(f.max_pool2d(self.conv1(x), 2))
		x = f.relu(f.max_pool2d(self.conv2_bn(self.conv2(x)), 2))
		x = f.relu(f.max_pool2d(self.conv3_bn(self.conv3(x)), 2))
		# print(x.shape)
		x = x.view(-1, self.flatshape)
		# print(x.shape)
		x = f.relu(self.fc1_bn(self.fc1(x)))
		# x = f.relu(self.fc2_bn(self.fc2(x)))
		x = self.fc2(x)
		# x = f.relu(self.fc3_bn(self.fc3(x)))
		# x = self.fc4(x)

		return x

