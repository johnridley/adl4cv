import torch.nn as nn
import torch

# Spatial Pyramid Architecture
class desc(nn.Module):
    
    def __init__(self):
        super(desc, self).__init__()
        self.input_channels = 4
        self.channels = 2
        
        # 306 X 92 X 4
        self.conv1 = ConvInitial(self.input_channels,self.channels*4,1,1,0) # 306 X 92 X 32
        self.conv3 = nn.Sequential(
                                ConvInitial(self.input_channels,self.channels*4,1,1,0),
                                nn.AvgPool2d(2,stride=2,padding=0),
                                ConvInitial(self.channels*4,self.channels*4,3,1,1)
                                ) # 153 X 46 X 32
        self.conv5 = nn.Sequential(
                                ConvInitial(self.input_channels,self.channels*2,1,1,0),
                                nn.AvgPool2d(3,stride=3,padding=0),
                                ConvInitial(self.channels*2,self.channels*2,3,1,1),
                                ConvInitial(self.channels*2,self.channels*4,1,1,0),
                                ConvInitial(self.channels*4,self.channels*4,3,1,1)
                                ) # 102 X 30 X 32
#        self.conv7 = nn.Sequential(
#                                ConvInitial(self.input_channels,self.channels,1,1,0),
#                                ConvInitial(self.channels,self.channels,3,2,1),
#                                ConvInitial(self.channels,self.channels,3,2,1)
#                                ) # 77 X 23 X 32
        
        self.inception1 = Inception(self.channels*4,self.channels*4) # l X h X (8*4*3)
        
        self.convA = nn.Sequential(
                                ConvInitial(self.channels*12,self.channels*12,1,1,0),
                                nn.AvgPool2d(3,stride=3,padding=0),
                                ConvInitial(self.channels*12,self.channels*12,3,1,1),
                                ConvInitial(self.channels*12,self.channels*12,1,1,0),
                                ConvInitial(self.channels*12,self.channels*12,3,1,1)
                                ) # 102 X 30 X 128
        self.convB = nn.Sequential(
                                ConvInitial(self.channels*12,self.channels*24,1,1,0),
                                nn.AvgPool2d(2,stride=2,padding=0),
                                ConvInitial(self.channels*24,self.channels*24,3,1,1)
                                ) # 77 X 23 X 128
        
        self.convC = nn.Sequential(
                                ConvInitial(self.channels*24,self.channels*48,1,1,0),
                                nn.AvgPool2d(2,stride=2,padding=0),
                                ConvInitial(self.channels*48,self.channels*48,3,1,1),#51 15
                                nn.AvgPool2d(2,stride=2,padding=0),
                                ConvInitial(self.channels*48,self.channels*96,1,1,0),
                                ConvInitial(self.channels*96,self.channels*96,3,1,1),#25 7
                                nn.AvgPool2d(2,stride=2,padding=0),
                                ConvInitial(self.channels*96,self.channels*96,3,1,1)#13 3
                                ) # 13 X 4 X 512
        self.convD = nn.Sequential(
                                ConvInitial(self.channels*24,self.channels*48,1,1,0),
                                ConvInitial(self.channels*48,self.channels*48,3,1,1),#25 7
                                nn.AvgPool2d(3,stride=3,padding=0),
                                ConvInitial(self.channels*48,self.channels*96,1,1,0),
                                ConvInitial(self.channels*96,self.channels*96,3,1,1),
                                nn.AvgPool2d(2,stride=2,padding=0),
                                ConvInitial(self.channels*96,self.channels*96,3,1,1)#13 3
                                ) # 13 X 4 X 512
        
        # 13 X 4 X (512*2)
        self.fc = nn.Sequential(
                                nn.Linear(13824,2048,bias=True),
                                nn.ReLU(inplace=True),
                                nn.Linear(2048,128,bias=True),
                                nn.ReLU(inplace=True),
                                nn.Linear(128,24)
                                )   
    def forward(self, image, heatmap):
        img = torch.cat((image, heatmap), dim=1)
        a = self.conv1(img); b = self.conv3(img); c = self.conv5(img)#; d = self.conv7(img)
        a = self.inception1(a); b = self.inception1(b); c = self.inception1(c)#; d = self.inception1(d)
        a = self.convA(a); b = self.convB(b)
        a = [a, c]; a = torch.cat(a, 1)#; b = [b, d]; b = torch.cat(b, 1)
        a = self.convC(a); b = self.convD(b)
        a = [a, b]; a = torch.cat(a, 1)
        a = a.view(a.size(0), -1); a = self.fc(a)
        return a
    @property
    def is_cuda(self):
        return next(self.parameters()).is_cuda

class ConvInitial(nn.Module):
    def __init__(self, ip_channel, op_channel, kernel, stride, padding):
        super(ConvInitial, self).__init__()
        self.conv = nn.Conv2d(ip_channel,op_channel,kernel,stride=stride,padding=padding,bias=True)
        nn.init.xavier_uniform_(self.conv.weight)
        self.bn = nn.BatchNorm2d(op_channel)
        nn.init.constant_(self.bn.weight, 1)
        nn.init.constant_(self.bn.bias, 0)
        self.non_lin = nn.ReLU(inplace=True)
    def forward(self, x):
        x = self.conv(x); x = self.bn(x); x = self.non_lin(x)
        return x

class Inception(nn.Module):
    def __init__(self, in_channel, out_channel):
        super(Inception, self).__init__()
        self.branch1 = nn.Sequential(
                                    ConvInitial(in_channel,out_channel,1,stride=1,padding=0)
                                    )
        self.branch3 = nn.Sequential(
                                    ConvInitial(in_channel,out_channel,1,stride=1,padding=0),
                                    ConvInitial(out_channel,out_channel,3,stride=1,padding=1)
                                    )
        self.branch5 = nn.Sequential(
                                    ConvInitial(in_channel,out_channel,3,stride=1,padding=1),
                                    ConvInitial(out_channel,out_channel,3,stride=1,padding=1)
                                    )
        
    def forward(self, x):
        b1 = self.branch1(x); b2 = self.branch3(x); b3 = self.branch5(x)
        b1 = torch.add(b1,x); b2 = torch.add(b2,x); b3 = torch.add(b3,x)
        outputs = [b1, b2, b3]
        return torch.cat(outputs, 1)
