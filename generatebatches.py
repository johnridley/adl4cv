from KITTICat.KITTIData import *
from KITTICat.KITTIBatch import *

import pathlib
import numpy as np

data = KITTIData()
data.loadFrom(pathlib.Path('.'))

# Setup the generator with desired difficulty parameters and splits
generator = KITTIBatchGenerator(data.getObjects(label="Car", occluded=2, truncated=0.3), data, 0.8, 0.1, selector=['images', '2D', '3D', 'points', 'reduced-points'])

# Create three sets
for batch in ['train', 'validate', 'test']:
	print(f'Making {batch} set')
	generator.asRole(batch)
	writer = KITTIBatchWriter(generator, batch)
	writer.write()