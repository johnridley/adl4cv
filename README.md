# Lifting 2D Detection to 3D

## Processing, training and visualisation apparatuses
### Advanced Deep Learning for Computer Vision (SS19)
### John Ridley & Deepan Das

---

## Notebooks
 - Preprocess: Testing for data manipulation
 - TrainTest: Training and testing of lifting pipeline
 - TrainGAN: Training of GAN architectures

## Scripts
 - convertdata.py: Ingests the provided KITTI data (in /KITTI) and prepares it for batching
 - generatebatches.py: Converts the pre-prepared data into batches for use with TrainTest
 - test2D.py: Evaluates trained networks on the provided 2dlifted and 2doutput folders from 2D detectors
 - visualise.py: Runs through samples and visualises detection in 3D

## Custom Libraries
 - KITTICat: Processing, validation, batching and organisational tools for the KITTI dataset
 - LiftVision: Visualisation tools
 - Model: Network architectures
 

