import pathlib, sys, logging

from KITTICat.KITTIConvert import KITTIConverter

try:
	# Convert data from argument provided path
	importer = KITTIConverter(sys.argv[1])
	data = importer.loadTrain()
	data.saveTo(pathlib.Path('.'))
except IndexError:
	logging.error('No KITTI path argument specified')
