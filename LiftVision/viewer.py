from math import pi, sin, cos
import os
 
from panda3d.core import *
loadPrcFileData("", "framebuffer-multisample 1") 
loadPrcFileData("", "multisamples 4") 
loadPrcFileData("", "window-title LiftVision") 
from direct.showbase.ShowBase import ShowBase
from direct.showbase.Transitions import Transitions
from direct.gui.OnscreenImage import OnscreenImage
from direct.gui.OnscreenText import OnscreenText
from direct.task import Task
 
MODULEPATH = os.path.dirname(os.path.realpath(__file__))

from .models import Car, Road

class LiftVision(ShowBase):
	# Configures the 3D environment of the visualiser
	def __init__(self, driver=None):
		ShowBase.__init__(self)
		self.setBackgroundColor((0, 0, 0))

		self.driver = driver
		self.meanDist = 20
		self.rotationSpeed = 20

		imageScale = 0.23
		self.imageObject = OnscreenImage(image = f'{MODULEPATH}/textures/image.png', pos=(3.3*imageScale, 0, -1*imageScale), scale=(3.3*imageScale, 1, 1*imageScale), parent=self.a2dTopLeft)
		self.textObject = OnscreenText(text='', align=TextNode.ARight, fg=(1, 1, 1, 1), pos=(-0.12, -0.1), scale=0.07, mayChange=True, parent=self.a2dTopRight)
		self.textObject.setFont(loader.loadFont(f'{MODULEPATH}/textures/lato.ttf', color=(1,1,1,1), renderMode=TextFont.RMSolid))

		cm = CardMaker('card')
		cm.setFrame(-256, 256, -256, 256)
		self.ground = cm.generate()
		self.ground = render.attachNewNode(self.ground)
		self.ground.setP(self.ground, -90)

		self.ground.setPos(0, 0, -1)
		self.ground.setTexture(loader.loadTexture(f"{MODULEPATH}/textures/road.jpg"))
		self.ground.getTexture().setWrapU(Texture.WM_repeat)
		self.ground.getTexture().setWrapV(Texture.WM_repeat)
		self.ground.setTexScale(TextureStage.getDefault(), 20)

		self.light = render.attachNewNode(Spotlight("Spot"))
		self.light.setPos(0, -80, 250)
		self.light.lookAt((0, 30, 0))
		self.light.node().setScene(render)
		self.light.node().setShadowCaster(True, 4096, 4096)
		self.light.node().getLens().setFov(60)
		self.light.node().getLens().setNearFar(10, 300)
		self.render.setLight(self.light)
		self.render.setShaderAuto()


		self.road = Road(self)

		self.transitions = Transitions(self.loader)

		if self.driver is None:
			print("No driver provided, faking some data")
			
			self.road.renderCar(Car(), "Origin")
			self.road.renderRandom(20)

		else:
			self.driverIter = iter(driver)


		self.updated = False
		self.hidden = True
		self.taskMgr.add(self.renderFrame, "RenderFrame")


	def renderFrame(self, task):
		# Frame renderloop
		angleDegrees = (task.time * self.rotationSpeed) % 360
		angleRadians = angleDegrees * (pi / 180.0)

		if angleDegrees < 10 and not self.updated and self.driver is not None:
			self.transitions.fadeOut(t=0)
			self.updated = True
			image, cars, self.meanDist = next(self.driverIter)
			self.imageObject.setImage(image)
			self.road.destroy()
			self.road.renderCar(Car(), "Origin")
			self.road.render(cars)

			self.textObject.setText(str(self.driver))
			self.transitions.fadeIn(t=2.5)
			self.hidden = False

		if angleDegrees > 10:
			self.updated = False

		if angleDegrees > 320 and not self.hidden:
			self.transitions.fadeOut(t=1)
			self.hidden = True

		
		self.camera.setPos(50 * sin(angleRadians), -50.0 * cos(angleRadians) + self.meanDist, 10)
		self.camera.lookAt((0, self.meanDist, 0))
		self.road.labelsFaceCamera(task)
		return Task.cont
