from math import pi
import numpy as np
import cv2, random, torch, os, time

from .models import Car

class LiftDriver:
	# Wrapper for the lifting network during visualisation
	IMGPATH = '/tmp/'
	def __init__(self, model, dataset, batchgen, validator):
		self.model = model
		self.model.eval()
		self.dataset = dataset
		self.batchgen = batchgen
		self.batch = list(batchgen)[0]
		self.validator = validator

		self.imageids = list(set(self.batch['imageids'].astype(int)))
		random.shuffle(self.imageids)
		self.prevImage = None
		self.str = "LiftVision"

	def addNoise(self, boxes):
		# Add noise to the 2D detections
		height = boxes[:, 3]  - boxes[:, 1]
		width = boxes[:, 2]  - boxes[:, 0]
		area = height * width

		hStd = area * 0.075 / height
		vStd = area * 0.075 / width
		noise = np.stack([np.random.normal(loc=0, scale=hStd), np.random.normal(loc=0, scale=vStd),
							np.random.normal(loc=0, scale=hStd), np.random.normal(loc=0, scale=vStd)]).T
		noisy = boxes + noise

		return noisy

	def boxToHeatmap(self, boxes, shape):
		# Convert bounding boxes to heatmaps
		heatmaps = np.zeros(shape)
		scaled = (boxes * self.dataset.SCALE).astype(int)
		for i in range(heatmaps.shape[0]):
			heatmaps[i, scaled[i, 1]:scaled[i, 3], scaled[i, 0]:scaled[i, 2]] = 1
		    
		return heatmaps

	def __iter__(self):
		self.imageIter = iter(self.imageids)
		return self

	def __next__(self):
		# Sample the next image
		imageID = next(self.imageIter)
		objectIDs = np.argwhere(self.batch['imageids'] == imageID)

		normImages = self.batch['norm-images'][objectIDs]
		normImages = normImages.reshape(len(objectIDs), *normImages.shape[2:])
		img = torch.from_numpy(normImages).permute(0, 3, 1, 2).float()

		noisyBoxes = self.batch['2DBoxes'][objectIDs]
		noisyBoxes = noisyBoxes.reshape(noisyBoxes.shape[0], -1)
		noisyHeatmap = self.boxToHeatmap(noisyBoxes, normImages.shape[0:3])
		heatmap = torch.from_numpy(np.expand_dims(noisyHeatmap, axis=3)).permute(0, 3, 1, 2).float()

		with torch.no_grad():
			start = time.time()
			outputs = self.model(img, heatmap)
			end = time.time()
			results = outputs.cpu().numpy()
		
		points = self.batch['points'][objectIDs]
		points = points.reshape(len(objectIDs), *points.shape[2:])
		scores, IOUs, boxes, gtBoxes = self.validator.validate(results, points, self.batchgen, weird=True)


		dimensions = self.batch['dimensions'][objectIDs].reshape(len(objectIDs), 3)
		locations = self.batch['location'][objectIDs].reshape(len(objectIDs), 3)[:, [0, 2, 1]]
		rotations = self.batch['rotation'][objectIDs].reshape(len(objectIDs), 1)

		cars = [Car(dimensions=[b.w, b.h, b.l], position=[b.c[0], b.c[2], b.c[1]], rotation=(b.angle), color=(1, 0, 0), text=str(iou)) for b, iou in zip(boxes, IOUs)]
		cars += [Car(dimensions=dim, position=loc, rotation=rot, color=(0, 1, 0), transparent=True) for dim, loc, rot in zip(dimensions, locations, rotations)]

		meanDist = np.mean(np.array([b.c for b in boxes]), axis=0)[2]

		image = self.dataset.images[imageID].copy()
		for b in boxes:
			image = b.render(image, self.dataset.matricies[imageID], (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)))

		if self.prevImage is not None:
			os.remove(self.prevImage)


		self.str  = f"{len(objectIDs)} Car{'s' if len(objectIDs) > 1 else ''}\n"
		self.str += f"Time: {(end - start)*1000:.2f}ms\n"
		self.str += f"Average IOU: {np.mean(IOUs)*100:.2f}%\n"
		self.str += f"KITTI Score: {np.mean(scores)*100:.2f}%"


		self.prevImage = LiftDriver.IMGPATH + str(random.randint(0, 1e10)) + '.png'
		cv2.imwrite(self.prevImage, image)
		return self.prevImage, cars, meanDist

	def __len__(self):
		return len(self.imageids)

	def __str__(self):
		return self.str
		