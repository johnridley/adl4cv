import os, random
from math import pi

from panda3d.core import *
from direct.task import Task

MODULEPATH = os.path.dirname(os.path.realpath(__file__))

class Car:
	# Representation of the car model
	MODELSIZE = (85, 59.001, 184.992)
	def __init__(self, dimensions=(2.125, 1.475, 4.625), position=(0, 0, -1), rotation=-(pi/2), color=(0, 0, 1), text="", transparent=False):
		self.scale = tuple([dimension/size for dimension, size in zip(dimensions, Car.MODELSIZE)])
		self.position = list(position)
		self.position[2] = -1
		self.rotation =  180.0 * (rotation / pi) + 90
		self.transparent = transparent
		self.color = color
		self.text = text

class Road:
	# Representation of the road
	def __init__(self, base):
		self.base = base
		self.carObjects = []

	def renderRandom(self, number):
		cars = []
		for i in range(number):
			dimensions = (random.uniform(1, 5), random.uniform(1, 2), random.uniform(1, 5))
			position = (random.uniform(-45, 45), random.uniform(1, 60), -1)
			rotation = random.uniform(-180, 180)
			color = (random.uniform(0, 1), random.uniform(0, 1), random.uniform(0, 1))
			transparent = random.choice([True, False])
			cars.append(Car(dimensions=dimensions, position=position, rotation=rotation, color=color, transparent=transparent))

		self.render(cars)

	def render(self, cars):
		carNum = 0
		for car in cars:
			self.renderCar(car, car.text)
			carNum += 1

	def renderCar(self, car, label):
		model = self.base.loader.loadModel(f'{MODULEPATH}/models/car.egg')
		model.setMaterial(Material())
		model.getMaterial().setSpecular((1, 1, 1, 0.4))
		model.getMaterial().setShininess(50.0)
		model.reparentTo(self.base.render)

		if car.transparent:
			model.setLightOff()
			model.setTransparency(1)
			model.setColor(*car.color, 0.6)

		else:
			model.setColor(*car.color, 1)


		model.setScale(*car.scale)
		model.setPos(*car.position)
		model.setP(model, -90)
		model.setR(model, 180 + car.rotation)

		text = TextNode('CarLabel')
		text.setFont(loader.loadFont(f'{MODULEPATH}/textures/lato.ttf', color=(1,1,1,1), renderMode=TextFont.RMSolid))
		text.setText(str(label))
		text.setAlign(TextNode.ACenter)

		textNode = self.base.render.attachNewNode(text)
		textNode.setLightOff()
		textNode.setScale(1, 0.1, 1)
		textNode.setPos((car.position[0], car.position[1], 1))

		self.carObjects.append((model, textNode))
		

	def destroy(self):
		for car, text in self.carObjects:
			car.removeNode()
			text.removeNode()

		self.carObjects = []

	def labelsFaceCamera(self, task):
		for car, text in self.carObjects:
			text.lookAt(self.base.camera, 0, 180, 0)

		return Task.cont
		